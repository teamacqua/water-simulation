////////////////////////////////////////////////////////////////////////////////
//
// (C) Andy Thomason 2012-2014
//
// Modular Framework for OpenGLES2 rendering on multiple platforms.
//

namespace octet {
  /// Scene containing a box with octet.
  class water_simulation : public app {
    // scene for drawing box
    ref<visual_scene> app_scene;
    ref<scene_node> water_node;

    bump_shader object_shader;
    bump_shader skin_shader;

    material *waterMat;
    mesh_builder waterMesh;
    camera_instance *cam;
    mesh *mWater;

    enum Resolutions {
      WIDTH = 66,
      HEIGHT = 66,
      PADDED_WIDTH = WIDTH + 2,
      PADDED_HEIGHT = HEIGHT + 2
    };

    float H[WIDTH][HEIGHT], V[WIDTH][HEIGHT], U[WIDTH][HEIGHT];
    float Hx[PADDED_WIDTH][PADDED_HEIGHT], Hy[PADDED_WIDTH][PADDED_HEIGHT], Ux[PADDED_WIDTH][PADDED_HEIGHT], Uy[PADDED_WIDTH][PADDED_HEIGHT], Vx[PADDED_WIDTH][PADDED_HEIGHT], Vy[PADDED_WIDTH][PADDED_HEIGHT];
    int n;
    float g;
    float dt;
    float dx;
    float dy;

  public:
    /// this is called when we construct the class before everything is initialised.
    water_simulation(int argc, char **argv) : app(argc, argv) {
    }

    /// this is called once OpenGL is initialized
    void app_init() {
      app_scene =  new visual_scene();
      app_scene->create_default_camera_and_lights();
      water_node = app_scene->add_scene_node();
      waterMat = new material(vec4(0,0,1,1));
    
      object_shader.init(false);
      skin_shader.init(true);

      setup();
      waterMesh.init();
      generateMesh();
      mWater = new mesh();
      waterMesh.get_mesh(*mWater);
      mWater->set_mode(GL_TRIANGLES);
      app_scene->add_mesh_instance(new mesh_instance(water_node, mWater, waterMat));

      cam = app_scene->get_camera_instance(0);
      cam->set_perspective(0, 45, 1, 10.0f, 1000.0f);
      scene_node *cnode = cam->get_node();
      cnode->access_nodeToParent().translate(20,10,50);
    }

    void setup(){
      n = 64;
      g = 9.8f;
      dt = 0.2f;
      dx = 1.0f;
      dy = 1.0f;
      
      for (int i = 0; i < 64; i++){
        for (int j = 0; j < 64; j++){
          Hx[i][j] = 0.0f;
          Ux[i][j] = 0.0f;
          Vx[i][j] = 0.0f;
          Hy[i][j] = 0.0f;
          Uy[i][j] = 0.0f;
          Vy[i][j] = 0.0f;       
        }   
      }
      for (int i = 0; i < 66; i++){
        for (int j = 0; j < 66; j++){
          H[i][j] = 1.0f;
          U[i][j] = 0.0f;
          V[i][j] = 0.0f;
        }
      }    
    }

    void boundaryConditions(){
      for (int i = 0; i < 66; i++){
        H[i][1]  = H[i][2];
        H[i][65] = H[i][64];
        H[1][i]  = H[2][i];
        H[65][i] = H[64][i];
        
        U[i][1]  = U[i][2];
        U[i][63] = U[i][62];
        U[1][i]  = U[2][i];
        U[63][i] = U[62][i];
        
        V[i][1]  = V[i][2];
        V[i][63] = V[i][62];
        V[1][i]  = V[2][i];
        V[63][i] = V[62][i];   
      }
    }

    void halfStep(){
      //x direction
      for (int i = 1; i < 64; i++){
        for (int j = 1; j < 63; j++){
        
          Hx[i][j] = (H[i+1][j+1] + H[i][j+1])/2.0f - 
                     (dt/(2.0f*dx))*(U[i+1][j+1] - U[i][j+1]);
          if (Hx[i][j] != Hx[i][j]){
            printf("Nan reached at Hx \n");          
          }
      
          Ux[i][j] = (U[i+1][j+1] + U[i][j+1])/2.0f - 
                     (dt/(2.0f*dx))*(((U[i+1][j+1] * U[i+1][j+1]) / H[i+1][j+1]) + 
                     (g/2.0f) * H[i+1][j+1] * H[i+1][j+1] - 
                     ((U[i][j+1] * U[i][j+1]) / H[i][j+1] + 
                     (g/2.0f) * H[i][j+1] * H[i][j+1]));
          if (Ux[i][j] != Ux[i][j]){
            printf("Nan reached at Ux \n");          
          }

          Vx[i][j] = (V[i+1][j+1] + V[i][j+1])/2.0f - 
                     (dt/(2.0f*dx))*(((U[i+1][j+1] * V[i+1][j+1]) / H[i+1][j+1]) -
                     ((U[i][j+1] * V[i][j+1]) / H[i][j+1]));  
          if (Vx[i][j] != Vx[i][j]){
            printf("Nan reached at Vx \n");          
          }      
        }   
      }
  
      //y direction
      for (int i = 1; i < 63; i++){
        for (int j = 1; j < 64; j++){
      
          Hy[i][j] = (H[i+1][j+1] + H[i+1][j])/2.0f - 
                     (dt/(2.0f*dy))*(V[i+1][j+1] - V[i+1][j]);
          if (Hy[i][j] != Hy[i][j]){
            printf("Nan reached at Hy \n");          
          }

          Uy[i][j] = (U[i+1][j+1] + U[i+1][j])/2.0f - 
                     dt/(2.0f*dy)*((V[i+1][j+1] * U[i+1][j+1] / H[i+1][j+1]) - 
                     (V[i+1][j] * U[i+1][j] / H[i+1][j]));
          if (Uy[i][j] != Uy[i][j]){
            printf("Nan reached at Uy \n");          
          }
          
          Vy[i][j] = (V[i+1][j+1] + V[i+1][j])/2.0f - 
                     dt/(2.0f*dy)*((V[i+1][j+1] * V[i+1][j+1] /H[i+1][j+1] + 
                     (g/2.0f) * H[i+1][j+1] * H[i+1][j+1]) - 
                     (V[i+1][j] * V[i+1][j] /H[i+1][j] + 
                     (g/2.0f)*H[i+1][j] * H[i+1][j]));    
          if (Vy[i][j] != Vy[i][j]){
            printf("Nan reached at Vy \n");          
          }
        }
    
      }
    }

    void fullStep(){
      for (int i = 2; i < 64; i++){
        for (int j = 2; j < 64; j++){
          H[i][j] = H[i][j] - 
                    (dt/dx)*(Ux[i][j-1] - 
                    Ux[i-1][j-1]) - 
                    (dt/dy)*(Vy[i-1][j] - 
                    Vy[i-1][j-1]);
          H[i][j] *= 0.99f;
          if (H[i][j] != H[i][j]){
            setup();
            printf("Nan reached at H \n");
            break;            
          }
      
          U[i][j] = U[i][j] - 
                    (dt/dx)*(((Ux[i][j-1] * Ux[i][j-1]) / Hx[i][j-1] + 
                    (g/2.0f) * Hx[i][j-1] * Hx[i][j-1]) - 
                    ((Ux[i-1][j-1] * Ux[i-1][j-1]) / Hx[i-1][j-1] + 
                    (g/2.0f)*Hx[i-1][j-1] * Hx[i-1][j-1])) - 
                    (dt/dy)*(((Vy[i-1][j] * Uy[i-1][j]) / Hy[i-1][j]) - 
                    ((Vy[i-1][j-1] * Uy[i-1][j-1]) / Hy[i-1][j-1]));
          //U[i][j] *= 0.99f; 
          if (U[i][j] != U[i][j]){
            printf("Nan reached at U \n");          
          }
          V[i][j] = V[i][j] - 
                    (dt/dx)*(((Ux[i][j-1] * Vx[i][j-1]) / Hx[i][j-1]) - 
                    ((Ux[i-1][j-1] * Vx[i-1][j-1]) / Hx[i-1][j-1])) - 
                    (dt/dy)*(((Vy[i-1][j] * Vy[i-1][j]) / Hy[i-1][j] + 
                    (g/2.0f)*Hy[i-1][j] * Hy[i-1][j]) - 
                    ((Vy[i-1][j-1] * Vy[i-1][j-1]) / Hy[i-1][j-1] + 
                    (g/2.0f)*Hy[i-1][j-1] * Hy[i-1][j-1]));
          //V[i][j] *= 0.99f;  
          if (V[i][j] != V[i][j]){
            printf("Nan reached at V \n");          
          }
        }
      }
    }

    void generateMesh(){
      vec4 normals = vec4(0,1,0,0);
      int numVertices;
      for(int z = 0; z < 60; z+= 1){
        for(int x = 0; x < 60; x+= 1) {
          numVertices = waterMesh.numVertices();

          waterMesh.add_vertex(vec4(x*1.0f,     H[x][z],    z*1.0f,     1), normals, 0,0);
          waterMesh.add_vertex(vec4(x*1.0f+1.0f,H[x+1][z],  z*1.0f,     1), normals, 1,0);
          waterMesh.add_vertex(vec4(x*1.0f+1.0f,H[x+1][z+1],z*1.0f+1.0f,1), normals, 1,1);
          waterMesh.add_vertex(vec4(x*1.0f,     H[x][z+1],  z*1.0f+1.0f,1), normals, 0,1);
          //printf("%f\n",H[z][x]);
        
          waterMesh.add_index(numVertices);
          waterMesh.add_index(numVertices+1);
          waterMesh.add_index(numVertices+2);
          waterMesh.add_index(numVertices);
          waterMesh.add_index(numVertices+2);
          waterMesh.add_index(numVertices+3);
        }
      }
    }

    void modifyVertices(){
      waterMesh.delete_vertex();
      vec4 normals = vec4(0,1,0,0);
      for(int z = 0; z < 60; z+= 1){
        for(int x = 0; x < 60; x+= 1) {
          waterMesh.add_vertex(vec4(x*1.0f,     H[x][z],    z*1.0f,     1), normals, 0,0);
          waterMesh.add_vertex(vec4(x*1.0f+1.0f,H[x+1][z],  z*1.0f,     1), normals, 1,0);
          waterMesh.add_vertex(vec4(x*1.0f+1.0f,H[x+1][z+1],z*1.0f+1.0f,1), normals, 1,1);
          waterMesh.add_vertex(vec4(x*1.0f,     H[x][z+1],  z*1.0f+1.0f,1), normals, 0,1);
        }
      }
    }

    void cameraControl(){
      scene_node *cnode = cam->get_node();      
      float offset = 5.0f;
      if (is_key_down(key_left)) {
        cnode->access_nodeToParent().translate(-offset,0.0f,0.0f);
      }
      if (is_key_down(key_right)) {
        cnode->access_nodeToParent().translate(offset,0.0f,0.0f);
      }
      if (is_key_down(key_down)) {
        cnode->access_nodeToParent().translate(0.0f,00.0f,offset);
      }
      if (is_key_down(key_up)) {
        cnode->access_nodeToParent().translate(0.0f,0.0f,-offset);
      }
      
    }

    /// this is called to draw the world
    void draw_world(int x, int y, int w, int h) {
      int vx = 0, vy = 0;
      get_viewport_size(vx, vy);
      glClearColor(0, 0, 0, 1);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // allow Z buffer depth testing (closer objects are always drawn in front of far ones)
      glEnable(GL_DEPTH_TEST);

      boundaryConditions();
      halfStep();
      fullStep();
      modifyVertices();
      
      cameraControl();
      //simulate a drop
      if (is_key_down('E')) {
        for (int i = 15; i < 20; i++){
          for (int j = 15; j < 20; j++){
            H[i][j] = 3.0f;
          }
        }
      }

      // update matrices. assume 30 fps.
      app_scene->update(1.0f/30);

      waterMesh.get_mesh(*mWater);

      // draw the scene
      app_scene->render(object_shader, skin_shader, *cam, (float)vx / vy);     
      
    }  
  };
}
