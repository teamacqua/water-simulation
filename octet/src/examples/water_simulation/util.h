#ifndef UTILITIES_H_INCLUDED
#define UTILITIES_H_INCLUDED

namespace util {

  template <typename T>
  inline T square(const T& x) {
    return x * x;
  };

  // --

  template <typename T>
  T lerp(const T& begin, const T& end, float t) {
    return ((1.f - t) * begin) + (t * end);
  };
  
  // --

  // ^ p01 -- p11
  // |  |      |
  // |  |      |
  // V p00 -- p10
  //    U ---->
  template <typename T>
  T bilerp(const T& p00, const T& p10, const T& p11, const T& p01, float u, float v) {
    return lerp(lerp(p00, p10, u), lerp(p01, p11, u), v);
  };
  
  // --

  // VERTICES:
  //
  //    .h------g
  //  .' |    .'|
  // d---+--c'  |
  // |   |  |   |
  // |  ,e--+---f
  // |.'    | .'
  // a------b'
  // 
  // AXIS:
  //
  // v
  // |  
  // |  ,w
  // |.'
  // +------u
  template <typename T>
  T trilerp(const T& a, const T& b, const T& c, const T& d, const T& e, const T& f, const T& g, const T& h, float u, float v, float w) {
    return lerp(bilerp(a, b, c, d, u, v), bilerp(e, f, g, h, u, v), w);
  };
  
  // --

  template <typename T>
  T clamp(const T& value, const T& min, const T& max) {
    return std::max(min, std::min(max, value));
  };

  // --

  // Utility class which manages input.
  // Used to delay key-press.
  class InputManager {
  private:
    octet::app* _app;

    // The key delay amount per poll request
    unsigned int _keyDelay;

    // Delay accumulator for processing key presses
    unsigned int _delay;

    // Non-copyable entity
    InputManager(const InputManager&);
    InputManager& operator=(const InputManager&);

  public:
    typedef unsigned int keycode;

    InputManager(octet::app* app = NULL, unsigned int keyDelay = 5) :
      _app(app),
      _keyDelay(keyDelay),
      _delay(0) {
    };

    ~InputManager() {
    };

    void attach(octet::app* app) {
      _app = app;
      _delay = 0;
    };

    void setKeyDelay(unsigned int keyDelay) {
      _keyDelay = keyDelay;
      _delay = 0;
    };

    bool isKeyDown(keycode key) const {
      return _delay == 0 && _app != NULL && _app->is_key_down(key);
    };

    bool processKey(keycode key) {
      if (isKeyDown(key)) {
        _delay = _keyDelay;
        return true;
      };

      return false;
    };

    // Check for key-presses
    void poll() {
      _delay = (_delay == 0 ? 0 : _delay - 1);
    };
  };

  // --
    
  /**
    *  Utility class which handles mouse input and
    *  translates it into camera movement.
    */
  class MouseCameraManager {
  private:
    octet::app* _app;

    // octet's mouse_ball implementation for rotation of objects
    octet::mouse_ball _rotation;
    float _distance;
    float _sensitivity;

    // Mouse Wheel
    int _clicks;
    float _zoomFactor;
    float _maxZoom;
    float _minZoom;

    // Mouse Position
    int _x;
    int _y;
      
    MouseCameraManager(const MouseCameraManager&);
    MouseCameraManager& operator=(const MouseCameraManager&);
      
    /**
      *  Updates the camera to reflect recent mouse movement.
      *  @param cameraToWorld the camera matrix instance to update.
      */
    void updateMovement(octet::mat4t& cameraToWorld) {
      int x = 0;
      int y = 0;
      _app->get_mouse_pos(x, y);

      if ((_x != x || _y != y) && _app->is_key_down(octet::key_lmb)) {
        cameraToWorld.translate(-((float) (x - _x)), (float) (y - _y), 0.f);
      };

      _x = x;
      _y = y;
    };
      
    /**
      *  Updates the camera to reflect rotation.
      *  @param cameraToWorld the camera matrix instance to update.
      */
    void updateRotation(octet::mat4t& cameraToWorld) {
      _rotation.update(cameraToWorld);
    };
      
    /**
      *  Updates the camera to reflect recent mouse wheel zoom differences.
      *  @param cameraToWorld the camera matrix instance to update.
      */
    void updateZooming(octet::mat4t& cameraToWorld) {
      int clicks = _app->get_mouse_wheel();

      if (clicks != _clicks) {
        float distance = ((clicks - _clicks) * _zoomFactor);
        //setDistance(distance);

        cameraToWorld.translate(0.f, 0.f, -distance);
        cameraToWorld[3].z() = std::min(_maxZoom, std::max(_minZoom, cameraToWorld[3].z()));
      }

      _clicks = clicks;
    };

  public:
    explicit MouseCameraManager(octet::app* app = NULL) :
      _app(app),
      _rotation(),
      _distance(0.f),
      _sensitivity(360.f),
      _clicks(0),
      _zoomFactor(2.f),
      _minZoom(7.f),
      _maxZoom(500.f),
      _x(0), _y(0) {
      attach(_app);
    };

    ~MouseCameraManager() {
    };
      
    /**
      *  Attaches this manager instance with the
      *  specified app instance.
      *  @param app The app which requires input handling
      */
    void attach(octet::app* app) {
      _app = app;
      
      // Reset _x, _y, _clicks in relation to the current app being attached
      if (_app != NULL) {
        _app->get_mouse_pos(_x, _y);
        _clicks = _app->get_mouse_wheel();
      }

      _rotation.init(_app, _distance, _sensitivity);
    };
      
    /**
      *  Specifiy the sensitvity used for rotation.
      *  @param sensitivity rotation sensitivity
      */
    void setSensitivity(float sensitivity) {
      _sensitivity = sensitivity;
      _rotation.init(_app, _distance, _sensitivity);
    };
      
    /**
      * @return the currently specified rotation sensitivity
      */
    float getSensitivity() const {
      return _sensitivity;
    };
      
    /**
      *  Specifiy the distance used for rotation.
      *  @param distance rotation distance
      */
    void setDistance(float distance) {
      _distance = distance;
      _rotation.init(_app, _distance, _sensitivity);
    };
      
    /**
      *  @return the currently specified rotation distance
      */
    float getDistance() const {
      return _distance;
    };
      
    /**
      *  Specifiy the minimum zoom.
      *  @param minZoom the minimum zoom
      */
    void setMinZoom(float minZoom) {
      _minZoom = minZoom;
    };
      
    /**
      *  @return the currently specified minimum zoom
      */
    float getMinZoom() const {
      return _minZoom;
    };

    /**
      *  Specifiy the maximum zoom.
      *  @param maxZoom the maximum zoom
      */
    void setMaxZoom(float maxZoom) {
      _maxZoom = maxZoom;
    };
      
    /**
      *  @return the currently specified maximum zoom
      */
    float getMaxZoom() const {
      return _maxZoom;
    };

    /**
      *  Specifiy the zoom factor used for zoom.
      *  @param zoomFactor the zoom factor
      */
    void setZoomFactor(float zoomFactor) {
      _zoomFactor = zoomFactor;
    };
      
    /**
      *  @return the currently specified zoom factor
      */
    float getZoomFactor() const {
      return _zoomFactor;
    };

    /**
      *  Updates the provided camera instance to reflect
      *  recent changes in mouse movement.
      *  @param cameraToWorld the camera matrix instance to update
      */
    void update(octet::mat4t& cameraToWorld) {
      updateMovement(cameraToWorld);
      updateRotation(cameraToWorld);
      updateZooming(cameraToWorld);
    };
  };

  // --
  
} // namespace util

#endif // UTILITIES_H_INCLUDED