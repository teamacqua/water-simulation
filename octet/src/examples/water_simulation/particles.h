////////////////////////////////////////////////////////////////////////////////
//
// Particle System
//
// Brian Gatt             [ma301bg]
//
// Reference: http://www-users.cselabs.umn.edu/classes/Fall-2013/csci5980/slides/03ParticleSystems.pdf
// Reference: http://www.pixar.com/companyinfo/research/pbm2001/pdf/slidesc.pdf
// Reference: http://www.pixar.com/companyinfo/research/pbm2001/pdf/notesc.pdf
// Reference: https://www.lri.fr/~mbl/ENS/IG2/devoir2/files/docs/fuzzyParticles.pdf
// Reference: http://buildnewgames.com/particle-systems/
//
////////////////////////////////////////////////////////////////////////////////

namespace octet {
  namespace particles {
    namespace util {

      template <typename T>
      class Pool {
      private:
        // Wrapper for the value type with an additional 'enabled' flag
        struct Element {
          T elem;
          bool enabled;
          
          Element() :
            enabled(true) {
          };
            
          Element(const Element& rhs) :
            elem(rhs.elem),
            enabled(rhs.enabled) {
          };

          explicit Element(const T& elem) :
            elem(elem),
            enabled(true) {
          };

          Element& operator=(const Element& rhs) {
            if (&rhs == this) {
              return *this;
            }

            elem = rhs.elem;
            enabled = rhs.enabled;

            return *this;
          };
        };

        octet::dynarray<Element> _pool;

        // NOTE This is a list of free indices in the pool. mesh_particle_system.h
        // coaleces this data structure as a linked list inside the wrapper container
        // Element by storing the next index which is free.
        std::queue<size_t> _free;

        size_t _size;

        // Finds the first enabled element in the pool
        size_t getInitialElement() const {
          size_t start = 0;
          while (start < _pool.size() && !_pool[start].enabled) {
            ++start;
          }

          return start;
        };

      public:
        template <typename J>
        class Iterator {
        private:
          Pool* _pool;
          size_t _index;

          friend class Pool;

          Iterator(Pool* pool, size_t index) :
            _pool(pool),
            _index(index) {
          };

          octet::dynarray<Element>& pool() {
            return _pool->_pool;
          };

        public:
          typedef J value_type_t;

          Iterator& operator++() {
            ++_index;
            while (_index < pool().size() && !pool()[_index].enabled) {
              ++_index;
            }

            return *this;
          };

          Iterator& operator--() {
            --_index;
            while (_index > -1 && !pool()[_index].enabled) {
              --_index;
            }

            return *this;
          };
          
          value_type_t& operator*() {
            assert(pool()[_index].enabled);
            return _pool()[_index].elem;
          };
          
          value_type_t* operator->() {
            assert(pool()[_index].enabled);
            return &(pool()[_index].elem);
          };
          
          bool operator==(const Iterator& rhs) const {
            return _pool == rhs._pool && _index == rhs._index;
          };

          bool operator!=(const Iterator& rhs) const {
            return !operator==(rhs);
          };
        };

        typedef T value_type_t;
        typedef Iterator<T> iterator_t;

        Pool() :
          _size(0) {
        };

        void clear() {
          _pool.reset();
          
          while (!_free.empty()) {
            _free.pop();
          }

          _size = 0;
        };
        
        void reserve(size_t size) {
          _pool.reserve(size);
        };

        size_t size() const {
          return _size;
        };

        const value_type_t& operator[](size_t index) const {
          return _pool[index].elem;
        };

        value_type_t& operator[](size_t index) {
          return _pool[index].elem;
        };

        void remove(iterator_t& iterator) {
          remove(iterator._index);
        };

        void remove(size_t index) {
          if (index < _pool.size()) {
            _pool[index].enabled = false;
            _free.push(index);
            --_size;
          }
        };

        size_t add(const value_type_t& item) {
          size_t index = _pool.size();

          if (_free.empty()) {
            // Pool is allowed to grow indefinetely
            _pool.push_back(Element(item));
          } else {
            index = _free.front();

            // Reuse free slots
            _pool[index] = Element(item);
            _free.pop();
          }

          ++_size;

          return index;
        };
        
        iterator_t begin() {
          return iterator_t(this, getInitialElement());
        };

        iterator_t end() {
          return iterator_t(this, _pool.size());
        };
      };

      // Reference: http://www-users.cselabs.umn.edu/classes/Fall-2013/csci5980/slides/03ParticleSystems.pdf [Slide 17]
      octet::vec2 getRandomPointInRectangle(octet::random& r, float width, float length) {
        return octet::vec2(r.get(0, 1) * width, r.get(0, 1) * length);
      };

      octet::vec2 getRandomPointInRectangle(float width, float length) {
        return octet::vec2(((float) ::rand() / (float) RAND_MAX) * width, ((float) ::rand() / (float) RAND_MAX) * length);
      };

    } // namespace util

    class Particle : public octet::resource {
    private:
      octet::vec3 _position;
      octet::vec3 _velocity;
      octet::vec3 _force;
      float _mass;

      unsigned int _lifetime;

    public:
      Particle() :
        _position(0.f),
        _velocity(0.f),
        _force(0.f),
        _mass(1.0f),
        _lifetime(0) {
      };

      const octet::vec3& getPosition() const {
        return _position;
      };

      octet::vec3& getPosition() {
        return _position;
      };

      const octet::vec3& getVelocity() const {
        return _velocity;
      };

      octet::vec3& getVelocity() {
        return _velocity;
      };

      const octet::vec3& getForce() const {
        return _force;
      };

      octet::vec3& getForce() {
        return _force;
      };

      float getMass() const {
        return _mass;
      };

      void setMass(float mass) {
        _mass = mass;
      };

      octet::vec3 getAcceleration() const {
        return _force / _mass;
      };

      void accelerate(const octet::vec3& acceleration) {
        _force += _mass * acceleration;
      };

      // Lifetime is calculated in terms of update frames
      unsigned int getLifetime() const {
        return _lifetime;
      };

      void setLifetime(unsigned int lifetime) {
        _lifetime = lifetime;
      };
    };

    class ForceObject : public octet::resource {
    public:
      virtual ~ForceObject() { };
      virtual void operator()(Particle* particle, float deltaTime) = 0;
    };

    class GravityForceObject : public ForceObject {
    private:
      octet::vec3 _gravity;

    public:
      explicit GravityForceObject(const octet::vec3& gravity = octet::vec3(0.0f, -9.8f, 0.0f)) :
        _gravity(gravity) {
      };
        
      const octet::vec3& getGravity() const {
        return _gravity;
      };
      
      octet::vec3& getGravity() {
        return _gravity;
      };

      void operator()(Particle* particle, float) {
        particle->accelerate(_gravity);
      };
    };

    class DragForceObject : public ForceObject {
    private:
      float _drag;

    public:
      explicit DragForceObject(float drag = 0.0f) :
        _drag(0.f) {
      };
        
      float getDrag() const {
        return _drag;
      };

      void setDrag(float drag) {
        _drag = drag;
      };

      void operator()(Particle* particle, float) {
        particle->getForce() += -_drag * particle->getVelocity();
      };
    };

    class DampedSpringForceObject : public ForceObject {
    private:
      octet::ref<Particle> _particleA;
      octet::ref<Particle> _particleB;

      float _kSpring;
      float _kDampening;
      float _restLength;

    public:
      DampedSpringForceObject(Particle* particleA, Particle* particleB) :
        _particleA(particleA),
        _particleB(particleB),
        _kSpring(1.f),
        _kDampening(1.f),
        _restLength(1.f) {
      };
        
      float getDampening() const {
        return _kDampening;
      };

      void setDampening(float dampening) {
        _kDampening = dampening;
      };

      float getSpringConstant() const {
        return _kSpring;
      };

      void setSpringConstant(float springConstant) {
        _kSpring = springConstant;
      };

      float getRestLength() const {
        return _restLength;
      };

      void setRestLength(float restLength) {
        _restLength = restLength;
      };

      void operator()(Particle*) {
        octet::vec3 positionDiff = _particleA->getPosition() - _particleB->getPosition();
        float length = positionDiff.length();

        octet::vec3 normalizedPositionDiff = positionDiff.normalize();

        octet::vec3 velocityDiff = _particleA->getVelocity() - _particleB->getVelocity();

        octet::vec3 force = -((_kSpring * (length - _restLength)) + (_kDampening * normalizedPositionDiff * velocityDiff)) * normalizedPositionDiff;

        _particleA->getForce() = force;
        _particleB->getForce() = -force;
      };
    };
    
    class ParticleSystem {
    public:
      typedef size_t ParticleIndex_t;

      class ParticleInstance {
      private:
        octet::ref<Particle> _particle;
        std::vector<octet::ref<ForceObject> > _forceObjects;

        friend class ParticleSystem;

        ParticleInstance(Particle* particle) :
          _particle(particle) {
        };

      public:
        // Required in order to store in containers...
        ParticleInstance() :
          _particle(NULL) {
        };

        ParticleInstance(const ParticleInstance& rhs) :
          _particle(rhs._particle),
          _forceObjects(rhs._forceObjects) {
        };
          
        ParticleInstance& operator=(const ParticleInstance& rhs) {
          if (&rhs == this) {
            return *this;
          }

          _particle = rhs._particle;
          _forceObjects = rhs._forceObjects;

          return *this;
        };
        
        Particle* getParticle() {
          return _particle;
        };

        const Particle* getParticle() const {
          return _particle;
        };
        
        std::vector<octet::ref<ForceObject> >& getForceObjects() {
          return _forceObjects;
        };
        
        const std::vector<octet::ref<ForceObject> >& getForceObjects() const {
          return _forceObjects;
        };

        void update(float deltaTime) {
          for (size_t i = 0; i < _forceObjects.size(); ++i) {
            _forceObjects[i]->operator()(_particle, deltaTime);
          }

          _particle->getPosition() += _particle->getVelocity() * deltaTime;
          _particle->getVelocity() += _particle->getAcceleration() * deltaTime;

          _particle->setLifetime(_particle->getLifetime() + 1);
        };
      };
      
    private:
      util::Pool<ParticleInstance> _particles;

    public:
      ParticleIndex_t addParticle() {
        return addParticle(new Particle());
      };

      ParticleIndex_t addParticle(Particle* particle) {
        return _particles.add(ParticleInstance(particle));
      };

      const Particle* getParticle(ParticleIndex_t index) const {
        return _particles[index]._particle;
      };
      
      Particle* getParticle(ParticleIndex_t index) {
        return _particles[index]._particle;
      };
      
      const std::vector<octet::ref<ForceObject> >& getForceObjects(ParticleIndex_t index) const {
        return _particles[index]._forceObjects;
      };

      std::vector<octet::ref<ForceObject> >& getForceObjects(ParticleIndex_t index) {
        return _particles[index]._forceObjects;
      };

      const util::Pool<ParticleInstance>& getParticles() const {
        return _particles;
      };

      void removeParticle(ParticleIndex_t particleIndex) {
        _particles.remove(particleIndex);
      };
        
      util::Pool<ParticleInstance>::iterator_t begin() {
        return _particles.begin();
      };
      
      util::Pool<ParticleInstance>::iterator_t end() {
        return _particles.end();
      };

      size_t size() const {
        return _particles.size();
      };

      void update(float deltaTime) {
        // Clear force accumulators
        for (util::Pool<ParticleInstance>::iterator_t i = _particles.begin(); i != _particles.end(); ++i) {
          i->_particle->getForce() = octet::vec3(0.0f);
        }

        // Apply forces to particles
        for (util::Pool<ParticleInstance>::iterator_t i = _particles.begin(); i != _particles.end(); ++i) {
          i->update(deltaTime);
        }
      };

    };
    
    class LifeForceObject : public ForceObject {
    private:
      ParticleSystem::ParticleIndex_t _index;
      ParticleSystem* _system;

      unsigned int _maxLifetime;

    public:
      explicit LifeForceObject(const ParticleSystem::ParticleIndex_t index, ParticleSystem* system) :
        _index(index),
        _system(system),
        _maxLifetime(100) {
      };
        
      unsigned int getMaxLifetime() const {
        return _maxLifetime;
      };

      void setMaxLifetime(unsigned int maxLifetime) {
        _maxLifetime = maxLifetime;
      };

      void operator()(Particle*, float) {
        Particle* particle = _system->getParticle(_index);

        if (particle->getLifetime() > _maxLifetime) {
          _system->removeParticle(_index);
        }
      };
    };
    
    class BillboardParticleRenderer {
    private:
      octet::vec2 _dimensions;

      octet::camera_instance* _camera;
      octet::vec2 _uv[4];

    public:
      enum UVCoord { UNDEFINED = -1, BOTTOM_RIGHT = 0, TOP_RIGHT = 1, TOP_LEFT = 2, BOTTOM_LEFT = 3 };

      BillboardParticleRenderer() :
        _dimensions(1.0f, 1.0f),
        _camera(NULL) {

          for (size_t i = 0; i < 4; ++i) {
            _uv[i] = octet::vec2(0.f);
          }

      };

      octet::camera_instance* getCameraInstance() const {
        return _camera;
      };

      void setCameraInstance(octet::camera_instance* camera) {
        _camera = camera;
      };
      
      const octet::vec2& getDimensions() const {
        return _dimensions;
      };
      
      octet::vec2& getDimensions() {
        return _dimensions;
      };

      octet::vec2 getUVCoord(const UVCoord coord) const {
        if (coord < BOTTOM_RIGHT || coord > BOTTOM_LEFT) {
          return octet::vec2(0.f);
        }

        return _uv[coord];
      };

      void setUVCoord(const UVCoord coord, const octet::vec2& uv) {
        if (coord < BOTTOM_RIGHT || coord > BOTTOM_LEFT) {
          return;
        }

        _uv[coord] = uv;
      };

      void setUVCoords(const octet::vec2& uvBottomLeft, const octet::vec2& uvTopRight) {
        _uv[BOTTOM_LEFT] = uvBottomLeft;
        _uv[TOP_LEFT] = octet::vec2(uvBottomLeft.x(), uvTopRight.y());

        _uv[TOP_RIGHT] = uvTopRight;
        _uv[BOTTOM_RIGHT] = octet::vec2(uvTopRight.x(), uvBottomLeft.y());
      };

      void render(ParticleSystem& system, octet::mesh& mesh) const {
        mesh.set_mode(GL_TRIANGLES);

        octet::gl_resource::rwlock vertexLock = octet::gl_resource::rwlock(mesh.get_vertices());
        octet::gl_resource::rwlock indexLock = octet::gl_resource::rwlock(mesh.get_indices());

        octet::mesh::vertex *vertex = (octet::mesh::vertex*)vertexLock.u8();
        unsigned int *index = indexLock.u32();
        
        octet::mat4t cameraToWorld = _camera->get_node()->calcModelToWorld();

        // Stretch the billboard by the specified dimensions along each cardinal axis
        octet::vec3 xOrientation = _dimensions.x() * cameraToWorld.x().xyz();
        octet::vec3 yOrientation = _dimensions.y() * cameraToWorld.y().xyz();
        
        // Normal of the billboard will always face the camera's z-axis orientation
        octet::vec3 normal = cameraToWorld.z().xyz();

        size_t vertexOffset = 0;
        size_t indexOffset = 0;

        for (util::Pool<ParticleSystem::ParticleInstance>::iterator_t i = system.begin();
              i != system.end();
              ++i) {

              vertex[vertexOffset].pos = i->getParticle()->getPosition();
              vertex[vertexOffset].uv = getUVCoord(TOP_LEFT);
              vertex[vertexOffset].normal = normal;

              vertex[vertexOffset+1].pos = i->getParticle()->getPosition() + xOrientation;
              vertex[vertexOffset+1].uv = getUVCoord(TOP_RIGHT);
              vertex[vertexOffset+1].normal = normal;
                
              vertex[vertexOffset+2].pos = i->getParticle()->getPosition() - yOrientation;
              vertex[vertexOffset+2].uv = getUVCoord(BOTTOM_LEFT);
              vertex[vertexOffset+2].normal = normal;
                
              vertex[vertexOffset+3].pos = i->getParticle()->getPosition() - yOrientation + xOrientation;
              vertex[vertexOffset+3].uv = getUVCoord(BOTTOM_RIGHT);
              vertex[vertexOffset+3].normal = normal;

              // Triangle 1 in clockwise order
              index[indexOffset] = vertexOffset;
              index[indexOffset+1] = vertexOffset+1;
              index[indexOffset+2] = vertexOffset+2;

              // Triangle 2 in clockwise order
              index[indexOffset+3] = vertexOffset+1;
              index[indexOffset+4] = vertexOffset+3;
              index[indexOffset+5] = vertexOffset+2;

              vertexOffset += 4;
              indexOffset += 6;

        }

        mesh.set_num_vertices(vertexOffset);
        mesh.set_num_indices(indexOffset);
      };
    };

  } // namespace particles
} // namespace octet