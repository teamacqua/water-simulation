////////////////////////////////////////////////////////////////////////////////
//
// Water Simulation
//
// Brian Gatt             [ma301bg]
// Stefana Ovesia         [ma301so]
//
////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <queue>

#include "../../octet.h"

#include "util.h"
#include "particles.h"
#include "rain_material.h"
#include "water_simulation.h"

/// Create a box with octet
int main(int argc, char **argv) {
  const char* prefix = "../../../";

  for (int i = 0; i < argc; ++i) {
    if (::strcmp(argv[i], "--dir") == 0 || ::strcmp(argv[i], "-d") == 0 && i + 1 < argc) {
      prefix = argv[i + 1];
    }
  }

  // path from bin\Debug to octet directory
  octet::app_utils::prefix(prefix);

  // set up the platform.
  octet::app::init_all(argc, argv);

  // our application.
  octet::water_simulation app(argc, argv);
  app.init();

  // open windows
  octet::app::run_all_apps();
}


