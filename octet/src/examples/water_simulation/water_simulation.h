////////////////////////////////////////////////////////////////////////////////
//
// Water Simulation
//
// Brian Gatt             [ma301bg]
// Stefana Ovesia         [ma301so]
//
////////////////////////////////////////////////////////////////////////////////

namespace octet {

  class water_simulation : public app {
  private:
    // Internal Classes
    
    class WaterForceObject : public particles::ForceObject {
    private:
      water_simulation* _parent;
      particles::ParticleSystem::ParticleIndex_t _index;
      
      typedef std::pair<float, float> height_index_t;

      height_index_t getHeightIndex(const octet::vec3& position) {
        return std::make_pair(
          position.x(),
          position.z()
        );
      };

      bool shouldModifyWaterBed(const height_index_t& index) {
        int x = (int) index.first;
        int z = (int) index.second;

        // Avoid boundary cells
        // Give a particle a 1 in 3 chance to interact with the water surface
        return (x > 0 && x < WIDTH - 1) && (z > 0 && z < HEIGHT - 1) && rand() < (RAND_MAX / 3);
      };

    public:
      WaterForceObject(water_simulation* parent, const particles::ParticleSystem::ParticleIndex_t index) :
        _parent(parent),
        _index(index) {
      };

      void operator()(particles::Particle*, float) {
        particles::Particle* particle = _parent->_rainSystem.getParticle(_index);

        height_index_t arrayIndex = getHeightIndex(particle->getPosition());
        int x = (int) arrayIndex.first;
        int z = (int) arrayIndex.second;

        float height = util::bilerp(
          _parent->H[x][z],
          _parent->H[x + 1][z],
          _parent->H[x + 1][z + 1],
          _parent->H[x][z + 1],
          arrayIndex.first - x,
          arrayIndex.second - z
        );

        if (particle->getPosition().y() < height) {
          if (shouldModifyWaterBed(arrayIndex)) {
            float force = particle->getMass() * 3.0f;

            _parent->H[x][z] += force;
            _parent->H[x + 1][z] += force;
            _parent->H[x + 1][z + 1] += force;
            _parent->H[x][z + 1] += force;
          }
          
          _parent->_rainSystem.removeParticle(_index);
        }
      };
    };

    class RainParticleEmitter {
    private:
      water_simulation* _parent;

      particles::ParticleSystem* _system;
      float _width;
      float _length;

      float _height;

      float _genRate;

      octet::ref<particles::GravityForceObject> _gravity;
      
      static octet::vec3& getMinVelocity() {
        static octet::vec3 min(0.f, -1.f, 0.f);
        return min;
      };

      static octet::vec3& getMaxVelocity() {
        static octet::vec3 max(0.f, 0.f, 0.f);
        return max;
      };

      static float getMinMass() {
        return 0.1f;
      };

      static float getMaxMass() {
        return 0.5f;
      };

      float rand() const {
        return (float) ::rand() / (float) RAND_MAX;
      };
      
      void _generate() {
        octet::vec2 position = particles::util::getRandomPointInRectangle(_width, _length);

        octet::ref<particles::Particle> particle = octet::ref<particles::Particle>(new particles::Particle());
        particle->getPosition().x() = position.x();
        particle->getPosition().z() = position.y();

        // Fixed height
        particle->getPosition().y() = _height;
        
        particle->getVelocity() = ::util::lerp(getMinVelocity(), getMaxVelocity(), rand());
        particle->setMass(::util::lerp(getMinMass(), getMaxMass(), rand()));

        particles::ParticleSystem::ParticleIndex_t index = _system->addParticle(particle);
    
        //octet::dynarray<octet::ref<ForceObject> >& forces = _system->getForceObjects(index);
        std::vector<octet::ref<particles::ForceObject> >& forces = _system->getForceObjects(index);
  
        // FIXME This is ugly...
        // error C2664: 'octet::containers::dynarray<item_t>::push_back' : cannot convert parameter 1 from 'octet::containers::ref<item_t>' to 'const octet::containers::ref<item_t> &'
        forces.push_back(_gravity.operator->());

        if (_parent != NULL) {
          octet::ref<WaterForceObject> water(new WaterForceObject(_parent, index));
          forces.push_back(water.operator->());
        }
      };

    public:
      RainParticleEmitter(particles::ParticleSystem* system, float width = 10.0f, float length = 10.0f, float height = 10.0f) :
        _parent(NULL),
        _system(system),
        _width(width),
        _length(length),
        _height(height),
        // generate particles at a default rate of 10 particles per second
        _genRate(10.0f),
        _gravity(new particles::GravityForceObject()) {
      };

      void setGenRate(float genRate) {
        _genRate = genRate;
      };
      
      float getGenRate() const {
        return _genRate;
      };

      void setWidth(float width) {
        _width = width;
      };
      
      float getWidth() const {
        return _width;
      };
      
      void setLength(float length) {
        _length = length;
      };

      float getLength() const {
        return _length;
      };
      
      void setHeight(float height) {
        _height = height;
      };

      float getHeight() const {
        return _height;
      };
      
      octet::vec3& getGravity() {
        return _gravity->getGravity();
      };

      const octet::vec3& getGravity() const {
        return _gravity->getGravity();
      };

      void setParent(water_simulation* parent) {
        _parent = parent;
      };
      
      water_simulation* getParent() const {
        return _parent;
      };
      
      void update(float deltaTime) {
        // Reference: http://www-users.cselabs.umn.edu/classes/Fall-2013/csci5980/slides/03ParticleSystems.pdf [Slide 16]
        float particlesToGenerate = _genRate * deltaTime;
        size_t particlesToGenerateFully = (size_t) ::floor(particlesToGenerate);

        if (rand() < (particlesToGenerate - particlesToGenerateFully)) {
          ++particlesToGenerateFully;
        }

        generate(particlesToGenerateFully);
      };

      void generate(size_t count = 1) {
        for (size_t i = 0; i < count; ++i) {
          _generate();
        }
      };
    };


  private:
    // Scene
    ref<visual_scene> app_scene;
    
    mesh_builder waterMesh;
    octet::ref<mesh> mWater;

    static float getGravity() {
      return -9.8f;
    };

    static float getWidth() {
      return 60.f;
    };

    static float getLength() {
      return 60.f;
    };

    static float getRainEmitHeight() {
      return 30.f;
    };

    enum Resolutions {
      WIDTH = 66,
      HEIGHT = 66,
      PADDED_WIDTH = WIDTH + 2,
      PADDED_HEIGHT = HEIGHT + 2
    };

    float H[WIDTH][HEIGHT], V[WIDTH][HEIGHT], U[WIDTH][HEIGHT];
    float Hx[PADDED_WIDTH][PADDED_HEIGHT], Hy[PADDED_WIDTH][PADDED_HEIGHT], Ux[PADDED_WIDTH][PADDED_HEIGHT], Uy[PADDED_WIDTH][PADDED_HEIGHT], Vx[PADDED_WIDTH][PADDED_HEIGHT], Vy[PADDED_WIDTH][PADDED_HEIGHT];
    int n;
    float g;
    float dt;
    float dx;
    float dy;

    octet::ref<octet::mesh> _rain;
    particles::ParticleSystem _rainSystem;
    RainParticleEmitter _rainEmitter;
    particles::BillboardParticleRenderer _rainRenderer;

    mat4t _originalCameraOrientation;
    util::MouseCameraManager _cameraManager;

    void update(float deltaTime) {
      _rainEmitter.update(deltaTime);
      _rainSystem.update(deltaTime);
      _rainRenderer.render(_rainSystem, *_rain);
    };

    void initialiseRain(visual_scene* scene) {
      
      // -- Initialise Scene

      octet::scene_node* node = scene->add_scene_node();

      _rain = new octet::mesh(0x2000, 0x2000);
      
      octet::ref<octet::image> image(new octet::image("assets/water/droplet.gif"));
      octet::ref<octet::material> material(new octet::rain_material(image));

      octet::ref<octet::mesh_instance> instance(new mesh_instance(node, _rain, material));

      scene->add_child(node);
      scene->add_mesh_instance(instance);

      // -- Initialise Renderer

      // Assign the default camera to the rain renderer
      _rainRenderer.setCameraInstance(scene->get_camera_instance(0));
      _rainRenderer.setUVCoords(octet::vec2(0.0f, 0.0f), octet::vec2(1.0f, 1.0f));
      
      // -- Initialise Emitter

      _rainEmitter.setParent(this);
      _rainEmitter.getGravity() = octet::vec3(0.0f, getGravity(), 0.0f);

      _rainEmitter.setWidth(getWidth());
      _rainEmitter.setLength(getLength());
      _rainEmitter.setHeight(getRainEmitHeight());

    };

    octet::mat4t& getCameraMatrix() {
      camera_instance* camera = app_scene->get_camera_instance(0);
      return camera->get_node()->access_nodeToParent();
    };

    void handleInput() {
      // Update camera in accordance to user input
      _cameraManager.update(getCameraMatrix());

      if (is_key_down(key_space)) {
        getCameraMatrix() = _originalCameraOrientation;
      }
    }
    
    void setup(){
      n = 64;
      g = -getGravity();
      dt = 0.15f;
      dx = 1.0f;
      dy = 1.0f;
      
      for (int i = 0; i < WIDTH; i++){
        for (int j = 0; j < HEIGHT; j++){
          Hx[i][j] = 0.0f;
          Ux[i][j] = 0.0f;
          Vx[i][j] = 0.0f;
          Hy[i][j] = 0.0f;
          Uy[i][j] = 0.0f;
          Vy[i][j] = 0.0f;       
        }   
      }
      for (int i = 0; i < PADDED_WIDTH; i++){
        for (int j = 0; j < PADDED_HEIGHT; j++){
          H[i][j] = 1.0f;
          U[i][j] = 0.0f;
          V[i][j] = 0.0f;
        }
      }    
    }

    void boundaryConditions(){
      for (int i = 0; i < 66; i++){
        H[i][1]  = H[i][2];
        H[i][65] = H[i][64];
        H[1][i]  = H[2][i];
        H[65][i] = H[64][i];
        
        U[i][1]  = U[i][2];
        U[i][63] = U[i][62];
        U[1][i]  = U[2][i];
        U[63][i] = U[62][i];
        
        V[i][1]  = V[i][2];
        V[i][63] = V[i][62];
        V[1][i]  = V[2][i];
        V[63][i] = V[62][i];   
      }
    }

    void halfStep(){
      //x direction
      for (int i = 1; i < n + 1; i++){
        for (int j = 1; j < n; j++){
        
          Hx[i][j] = (H[i+1][j+1] + H[i][j+1])/2.0f - 
                     (dt/(2.0f*dx))*(U[i+1][j+1] - U[i][j+1]);
          if (Hx[i][j] != Hx[i][j]){
            printf("Nan reached at Hx \n");          
          }
      
          Ux[i][j] = (U[i+1][j+1] + U[i][j+1])/2.0f - 
                     (dt/(2.0f*dx))*(((U[i+1][j+1] * U[i+1][j+1]) / H[i+1][j+1]) + 
                     (g/2.0f) * H[i+1][j+1] * H[i+1][j+1] - 
                     ((U[i][j+1] * U[i][j+1]) / H[i][j+1] + 
                     (g/2.0f) * H[i][j+1] * H[i][j+1]));
          if (Ux[i][j] != Ux[i][j]){
            printf("Nan reached at Ux \n");          
          }

          Vx[i][j] = (V[i+1][j+1] + V[i][j+1])/2.0f - 
                     (dt/(2.0f*dx))*(((U[i+1][j+1] * V[i+1][j+1]) / H[i+1][j+1]) -
                     ((U[i][j+1] * V[i][j+1]) / H[i][j+1]));  
          if (Vx[i][j] != Vx[i][j]){
            printf("Nan reached at Vx \n");          
          }      
        }   
      }
  
      //y direction
      for (int i = 1; i < n; i++){
        for (int j = 1; j < n + 1; j++){
      
          Hy[i][j] = (H[i+1][j+1] + H[i+1][j])/2.0f - 
                     (dt/(2.0f*dy))*(V[i+1][j+1] - V[i+1][j]);
          if (Hy[i][j] != Hy[i][j]){
            printf("Nan reached at Hy \n");          
          }

          Uy[i][j] = (U[i+1][j+1] + U[i+1][j])/2.0f - 
                     dt/(2.0f*dy)*((V[i+1][j+1] * U[i+1][j+1] / H[i+1][j+1]) - 
                     (V[i+1][j] * U[i+1][j] / H[i+1][j]));
          if (Uy[i][j] != Uy[i][j]){
            printf("Nan reached at Uy \n");          
          }
          
          Vy[i][j] = (V[i+1][j+1] + V[i+1][j])/2.0f - 
                     dt/(2.0f*dy)*((V[i+1][j+1] * V[i+1][j+1] /H[i+1][j+1] + 
                     (g/2.0f) * H[i+1][j+1] * H[i+1][j+1]) - 
                     (V[i+1][j] * V[i+1][j] /H[i+1][j] + 
                     (g/2.0f)*H[i+1][j] * H[i+1][j]));    
          if (Vy[i][j] != Vy[i][j]){
            printf("Nan reached at Vy \n");          
          }
        }
    
      }
    }

    void fullStep(){
      for (int i = 2; i < n + 1; i++){
        for (int j = 2; j < n + 1; j++){
          H[i][j] = H[i][j] - 
                    (dt/dx)*(Ux[i][j-1] - 
                    Ux[i-1][j-1]) - 
                    (dt/dy)*(Vy[i-1][j] - 
                    Vy[i-1][j-1]);
          if (H[i][j] != H[i][j]){
            printf("Nan reached at H \n");
          }
      
          U[i][j] = U[i][j] - 
                    (dt/dx)*(((Ux[i][j-1] * Ux[i][j-1]) / Hx[i][j-1] + 
                    (g/2.0f) * Hx[i][j-1] * Hx[i][j-1]) - 
                    ((Ux[i-1][j-1] * Ux[i-1][j-1]) / Hx[i-1][j-1] + 
                    (g/2.0f)*Hx[i-1][j-1] * Hx[i-1][j-1])) - 
                    (dt/dy)*(((Vy[i-1][j] * Uy[i-1][j]) / Hy[i-1][j]) - 
                    ((Vy[i-1][j-1] * Uy[i-1][j-1]) / Hy[i-1][j-1]));
          U[i][j] *= 0.99f; 
          if (U[i][j] != U[i][j]){
            printf("Nan reached at U \n");          
          }
          V[i][j] = V[i][j] - 
                    (dt/dx)*(((Ux[i][j-1] * Vx[i][j-1]) / Hx[i][j-1]) - 
                    ((Ux[i-1][j-1] * Vx[i-1][j-1]) / Hx[i-1][j-1])) - 
                    (dt/dy)*(((Vy[i-1][j] * Vy[i-1][j]) / Hy[i-1][j] + 
                    (g/2.0f)*Hy[i-1][j] * Hy[i-1][j]) - 
                    ((Vy[i-1][j-1] * Vy[i-1][j-1]) / Hy[i-1][j-1] + 
                    (g/2.0f)*Hy[i-1][j-1] * Hy[i-1][j-1]));
          V[i][j] *= 0.99f;  
          if (V[i][j] != V[i][j]){
            printf("Nan reached at V \n");          
          }
        }
      }
    }

    void generateMesh() {
      vec4 vertices[4];
      vec4 normals[4];
      vec2 uvs[4];
      
      const float deltaZ = 1.0f / getWidth();
      const float deltaX = 1.0f / getLength();

      int numVertices = 0;
      for(int z = 0; z < 60; z++) {
        for(int x = 0; x < 60; x++) {
          vertices[0] = vec4(x*1.0f,     H[x][z],    z*1.0f,     1);
          vertices[1] = vec4(x*1.0f+1.0f,H[x+1][z],  z*1.0f,     1);
          vertices[2] = vec4(x*1.0f+1.0f,H[x+1][z+1],z*1.0f+1.0f,1);
          vertices[3] = vec4(x*1.0f,     H[x][z+1],  z*1.0f+1.0f,1);

          normals[0] = (vertices[1] - vertices[0]).cross(vertices[3] - vertices[0]);
          normals[1] = (vertices[2] - vertices[1]).cross(vertices[0] - vertices[1]);
          normals[2] = (vertices[3] - vertices[2]).cross(vertices[1] - vertices[2]);
          normals[3] = (vertices[0] - vertices[3]).cross(vertices[2] - vertices[3]);
          
          uvs[0] = vec2(x * deltaX, z * deltaZ);
          uvs[1] = vec2(x * deltaX + deltaX, z * deltaZ);
          uvs[2] = vec2(x * deltaX + deltaX, z * deltaZ + deltaZ);
          uvs[3] = vec2(x * deltaX, z * deltaZ + deltaZ);

          for (size_t i = 0; i < 4; ++i) {
            waterMesh.add_vertex(vertices[i], normals[i], uvs[i].x(), uvs[i].y());
          }

          //waterMesh.add_vertex(vec4(x*1.0f,     H[x][z],    z*1.0f,     1), normals, 0,0);
          //waterMesh.add_vertex(vec4(x*1.0f+1.0f,H[x+1][z],  z*1.0f,     1), normals, 1,0);
          //waterMesh.add_vertex(vec4(x*1.0f+1.0f,H[x+1][z+1],z*1.0f+1.0f,1), normals, 1,1);
          //waterMesh.add_vertex(vec4(x*1.0f,     H[x][z+1],  z*1.0f+1.0f,1), normals, 0,1);
          //printf("%f\n",H[z][x]);

          waterMesh.add_index(numVertices);
          waterMesh.add_index(numVertices+1);
          waterMesh.add_index(numVertices+2);
          waterMesh.add_index(numVertices);
          waterMesh.add_index(numVertices+2);
          waterMesh.add_index(numVertices+3);

          numVertices += 4;
        }
      }
    }

    void modifyVertices(){
      waterMesh.delete_vertex();
      vec4 vertices[4];
      vec4 normals[4];
      vec2 uvs[4];
      
      const float deltaZ = 1.0f/60.0f;
      const float deltaX = deltaZ;

      int numVertices = 0;
      for(int z = 0; z < 60; z++) {
        for(int x = 0; x < 60; x++) {
          vertices[0] = vec4(x*1.0f,     H[x][z],    z*1.0f,     1);
          vertices[1] = vec4(x*1.0f+1.0f,H[x+1][z],  z*1.0f,     1);
          vertices[2] = vec4(x*1.0f+1.0f,H[x+1][z+1],z*1.0f+1.0f,1);
          vertices[3] = vec4(x*1.0f,     H[x][z+1],  z*1.0f+1.0f,1);

          normals[0] = (vertices[1] - vertices[0]).cross(vertices[3] - vertices[0]);
          normals[1] = (vertices[2] - vertices[1]).cross(vertices[0] - vertices[1]);
          normals[2] = (vertices[3] - vertices[2]).cross(vertices[1] - vertices[2]);
          normals[3] = (vertices[0] - vertices[3]).cross(vertices[2] - vertices[3]);
          
          uvs[0] = vec2(x * deltaX, z * deltaZ);
          uvs[1] = vec2(x * deltaX + deltaX, z * deltaZ);
          uvs[2] = vec2(x * deltaX + deltaX, z * deltaZ + deltaZ);
          uvs[3] = vec2(x * deltaX, z * deltaZ + deltaZ);

          for (size_t i = 0; i < 4; ++i) {
            waterMesh.add_vertex(vertices[i], normals[i], uvs[i].x(), uvs[i].y());
          }
        }
      }
    }

  public:
    /// this is called when we construct the class before everything is initialised.
    water_simulation(int argc, char **argv) :
      app(argc, argv),
      _rainEmitter(&_rainSystem) {
    }

    /// this is called once OpenGL is initialized
    void app_init() {
      app_scene = new visual_scene();
      app_scene->create_default_camera_and_lights();
      
      scene_node* water_node = app_scene->add_scene_node();
      ref<material> waterMat = new param_material(new image("assets/water/water.gif"));
    
      setup();
      waterMesh.init();
      generateMesh();
      mWater = new mesh();
      waterMesh.get_mesh(*mWater);
      mWater->set_mode(GL_TRIANGLES);
      app_scene->add_mesh_instance(new mesh_instance(water_node, mWater, waterMat));

      camera_instance* cam = app_scene->get_camera_instance(0);
      cam->set_perspective(0, 45, 1, 10.0f, 1000.0f);
      
      getCameraMatrix().translate(20,10,50);
      _originalCameraOrientation = getCameraMatrix();

      _cameraManager.attach(this);

      initialiseRain(app_scene);
    }

    /// this is called to draw the world
    void draw_world(int x, int y, int w, int h) {
      int vx = 0, vy = 0;
      get_viewport_size(vx, vy);
      glClearColor(0, 0, 0, 1);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // allow Z buffer depth testing (closer objects are always drawn in front of far ones)
      glEnable(GL_DEPTH_TEST);

      boundaryConditions();
      halfStep();
      fullStep();
      modifyVertices();
      
      //simulate a drop
      if (is_key_down('E')) {
        for (int i = 15; i < 20; i++){
          for (int j = 15; j < 20; j++){
            H[i][j] = 3.0f;
          }
        }
      }
      
      handleInput();

      // update matrices. assume 30 fps.
      app_scene->update(1.0f / 30.0f);

      waterMesh.get_mesh(*mWater);

      update(1.0f / 30.0f);
      //update(_args.getDeltaTime());

      // draw the scene
      app_scene->render((float)vx / vy);
      
    }  
  };
}
