////////////////////////////////////////////////////////////////////////////////
//
// Particle System
//
// Brian Gatt             [ma301bg]
//
////////////////////////////////////////////////////////////////////////////////

namespace octet {
namespace scene {

  class rain_material : public material {
  private:
    ref<alpha_texture_shader> _shader;
    ref<image> _droplet;

  public:
    rain_material(image* droplet) :
      _shader(new alpha_texture_shader()),
      _droplet(droplet) {
        _shader->init();
    };

    ~rain_material() {
    };
    
    virtual void render(const mat4t &modelToProjection, const mat4t &modelToCamera, vec4 *light_uniforms, int num_light_uniforms, int num_lights) {
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(_droplet->get_gl_target(), _droplet->get_gl_texture());

      _shader->render(modelToProjection, 0);
    };

    virtual void render_skinned(const mat4t &cameraToProjection, const mat4t *modelToCamera, int num_nodes, vec4 *light_uniforms, int num_light_uniforms, int num_lights) const {
      // NOT IMPLEMENTED
    };
  };

} // namespace scene
} // namespace octet